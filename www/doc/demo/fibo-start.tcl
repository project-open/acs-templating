ad_page_contract {
    @cvs-id $Id: fibo-start.tcl,v 1.2.28.1 2015/09/10 08:22:10 gustafn Exp $
} -properties {
    m:onevalue
} -query {
    m:naturalnum,notnull
}

# Local variables:
#    mode: tcl
#    tcl-indent-level: 4
#    indent-tabs-mode: nil
# End:
